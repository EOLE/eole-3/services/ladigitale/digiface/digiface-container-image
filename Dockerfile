FROM bitnami/git:latest as BUILD
COPY app-version /tmp/
RUN cd /tmp && \
    git clone https://gitlab.mim-libre.fr/EOLE/eole-3/services/ladigitale/digiface/digiface-sources.git && \
    cd digiface-sources && \
    git checkout $(cat ../app-version)
FROM hub.eole.education/proxyhub/node:latest as builder
RUN npm install -g npm@10.5.2
RUN npm install -g vite
RUN mkdir /src
COPY --from=BUILD /tmp/digiface-sources /src
WORKDIR src
RUN npm install
RUN npm run build
#
FROM hub.eole.education/proxyhub/library/nginx:alpine
COPY --from=builder src/dist /usr/share/nginx/html
COPY server_tokens.conf /etc/nginx/conf.d/
VOLUME /usr/share/nginx/html
VOLUME /etc/nginx
EXPOSE 80
